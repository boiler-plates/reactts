const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserWebpackPlugin = require("terser-webpack-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const HtmlMinimizerPlugin = require("html-minimizer-webpack-plugin");
const paths = require("../Helpers/path");
const { htmlFilePath } = require("../config.json");

const dev = [];

const prod = [
  new MiniCssExtractPlugin({
    filename: "[name].css",
  }),
  new CssMinimizerPlugin(),
  new HtmlMinimizerPlugin(),
];

function base(isDev) {
  const htmlFileName = `${htmlFilePath.entry}.${htmlFilePath.extention}`;

  return [
    new HtmlWebpackPlugin({
      template: paths.public(htmlFileName),
      filename: htmlFileName,
      favicon: "./assets/images/favicon.ico",
    }),
    new TerserWebpackPlugin(),
  ];
}

module.exports = function getPlugins(isDev) {
  const config = isDev ? dev : prod;
  return [...base(isDev), ...config];
};
