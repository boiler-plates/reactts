const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const loadersConfig = [
  {
    loader: "css-loader",
    options: {
      sourceMap: false,
      url: false,
    },
  },
  {
    loader: "less-loader",
    options: {
      sourceMap: false,
      lessOptions: {
        javascriptEnabled: true,
      },
    },
  },
];

const dev = [
  {
    test: /\.less$/,
    use: [...loadersConfig],
  },
];

const prod = [
  {
    test: /\.less$/,
    use: [{ loader: MiniCssExtractPlugin.loader }, ...loadersConfig],
  },
];

function base(isDev) {
  return [
    {
      test: /\.tsx?$/,
      use: "babel-loader",
      exclude: [/node_modules/],
    },
    {
      test: /\.(png|j?g|svg|gif)?$/,
      use: "file-loader?name=./images/[name].[ext]",
    },
  ];
}

module.exports = function getRules(isDev) {
  const config = isDev ? dev : prod;
  return [...base(isDev), ...config];
};
