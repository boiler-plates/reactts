import React from "react";
import ReactDOM from "react-dom";
import App from "./src/App";
import config from "./config.json";

ReactDOM.render(<App />, document.getElementById(config.DOMRender.id));
