import React from "react";
import Index from "./client/index";
import "css/index.less";

const App: React.FC = () => {
  return <Index />;
};

export default App;
