const { main } = require("./package.json");
const { modes } = require("./config.json");
const getRules = require("./settings/webpack.rules");
const getPlugins = require("./settings/webpack.plugins");
const paths = require("./helpers/path");
const getOptimizations = require("./settings/webpack.optimizations");
const getServer = require("./settings/webpack.server");

module.exports = function (env) {
  const isDev = !!env.development;
  const mode = isDev ? modes.dev : modes.prod;
  const entry = "../" + main;

  return {
    context: paths.src(),
    output: {
      path: paths.dist(),
      filename: "[name].js",
      clean: true,
    },
    mode,
    entry,
    module: {
      rules: getRules(isDev),
    },
    resolve: {
      extensions: [".js", ".jsx", ".ts", ".tsx"],
      alias: paths.alias,
    },
    plugins: getPlugins(isDev),
    optimization: getOptimizations(isDev),
    ...getServer(isDev),
  };
};
