const TerserWebpackPlugin = require("terser-webpack-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const HtmlMinimizerPlugin = require("html-minimizer-webpack-plugin");
const { optimization } = require("../config.json");

const dev = {};

const prod = {
  minimizer: [
    new TerserWebpackPlugin(),
    new CssMinimizerPlugin(),
    new HtmlMinimizerPlugin(),
    "...",
  ],
  runtimeChunk: {
    name: "runtime",
  },
  ...optimization.prod,
};

function base(isDev) {
  return {};
}

module.exports = function getOptimizations(isDev) {
  const config = isDev ? dev : prod;
  return { ...base(isDev), ...config };
};
