const { devServer } = require("../config.json");

const dev = [
  {
    devServer: {
      historyApiFallback: true,
      port: 9000,
      open: true,
      hot: true,
      compress: true,
      ...devServer,
    },
  },
];

const prod = [];

function base(isDev) {
  return [];
}

module.exports = function getServer(isDev) {
  const config = isDev ? dev : prod;
  return [...base(isDev), ...config];
};
