const path = require("path");
const tsconfig = require("../tsconfig.json");

const join = (dir) => path.join(__dirname, "../" + dir);

const tsConfigAlias = () => {
  const { paths } = tsconfig.compilerOptions;
  let result = {};

  Object.keys(paths)?.map(
    (index) => (result[index.slice(0, -2)] = join(paths[index][0].slice(2, -2)))
  );

  return result;
};

const paths = {
  root: (dir = "") => join(dir),
  public: (dir = "") => join("public/" + dir),
  src: (dir = "") => join("src/" + dir),
  dist: (dir = "") => join("dist/" + dir),
  alias: tsConfigAlias(),
};

module.exports = paths;
